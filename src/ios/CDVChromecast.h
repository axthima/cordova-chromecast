//
//  CDVStreaming.h
//  Tracktl
//
//  Created by Tracktl on 24/06/14.
//
//


#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <UIKit/UIKit.h>
#import <GoogleCast/GoogleCast.h>

@interface CDVChromecast : CDVPlugin<GCKDeviceScannerListener,
                                     GCKDeviceManagerDelegate,
                                     GCKMediaControlChannelDelegate,
                                     UIActionSheetDelegate>

- (void)toggleScreenMode:(CDVInvokedUrlCommand*)command; // en param l'id de la party, et callback si c bien ouvert
- (void)refresh:(CDVInvokedUrlCommand*)command;
- (void)disconnect:(CDVInvokedUrlCommand*)command;

@property(nonatomic, strong) GCKDeviceScanner* deviceScanner;
@property(nonatomic, strong) GCKDeviceManager* deviceManager;
@property(nonatomic, readonly) GCKMediaInformation* mediaInformation;
@property GCKDevice *selectedDevice;
@property GCKCastChannel *textChannel;
@property NSString *currentScreen;

@end
