var listeners = [];

module.exports = {
	register : function(callback){
		listeners.push(callback);
		return function(){
			listeners.splice(listeners.indexOf(callback), 1);
		};
	},
	refresh : function () {
		exec(function() {}, function(){}, "Chromecast", "refresh");
	},
	toggleScreenMode : function (id_party) {
		exec(function(){}, function(){}, "Chromecast", "toggleScreenMode", [id_party.toString()]);
	},
	disconnect : function() {
		exec(function(){}, function(){}, "Chromecast", "disconnect");
	},
	onStatus : function (connection_state, nb_devices) {
		for(var i = 0; i < listeners.length; i++) {
			listeners[i](connection_state, nb_devices);
		}
	}
};